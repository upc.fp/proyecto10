package Trabajo;
import java.util.Scanner;

public class Trabajo_02_IMC
{
    static int [] codigos = {101,102,103,104,105,106,107};
    static String [] trabajadores = {"Roxana","Mariana","Daphne","Jennifer","Lilia","Valeria","Susan"};

    static double [] IMC = {0,0,0,0,0,0,0};
    static int [] categoria_A =  {0,0,0,0,0,0,0}; // normal
    static int [] categoria_B =  {0,0,0,0,0,0,0}; // sobrepeso
    static int [] categoria_C =  {0,0,0,0,0,0,0}; // obesidad

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args)
    {
        int resultado, i, j, entro = 0;

        resultado = RegistrarDatos();

        if(resultado != 0)
        {
            System.out.println("\nRESULTADOS:");
            System.out.println("==========");

            System.out.println("Personal de la Categoria A (IMC < 15):");
            for (i = 1; i <= categoria_A.length; i++)
            {
                if (categoria_A[i - 1] != 0) {
                    for (j = 1; j <= trabajadores.length; j++) {
                        if (categoria_A[i - 1] == codigos[j - 1]) {
                            System.out.printf("%s tiene un IMC de: %.2f \n", trabajadores[j - 1], IMC[j - 1]);
                            j = trabajadores.length + 1;
                        }
                    }
                    entro = 1;
                }
                else
                    i = categoria_A.length + 1;
            }
            if(entro == 0)
                System.out.println("No hay trabajadores en esta categoria");

            entro = 0;
            System.out.println("\nPersonal de la Categoria B (15 <= IMC < 25):");
            for (i = 1; i <= categoria_B.length; i++)
            {
                if (categoria_B[i - 1] != 0) {
                    for (j = 1; j <= trabajadores.length; j++) {
                        if (categoria_B[i - 1] == codigos[j - 1]) {
                            System.out.printf("%s tienes un IMC de: %.2f \n", trabajadores[j - 1], IMC[j - 1]);
                            j = trabajadores.length + 1;
                        }
                    }
                    entro = 1;
                }
                else
                    i = categoria_B.length + 1;
            }
            if(entro == 0)
                System.out.println("No hay trabajadores en esta categoria");

            entro = 0;
            System.out.println("\nPersonal de la Categoria C (IMC >= 25):");
            for (i = 1; i <= categoria_C.length; i++)
            {
                if (categoria_C[i - 1] != 0) {
                    for (j = 1; j <= trabajadores.length; j++) {
                        if (categoria_C[i - 1] == codigos[j - 1]) {
                            System.out.printf("%s tiene un IMC de: %.2f \n", trabajadores[j - 1], IMC[j - 1]);
                            j = trabajadores.length + 1;
                        }
                    }
                    entro = 1;
                }
                else
                    i = categoria_C.length + 1;
            }
            if(entro == 0)
                System.out.println("No hay trabajadores en esta categoria");
        }
        else
            System.out.println("No se presento ningun trabajador");
    }

    static int RegistrarDatos()
    {
        int codigo = -1;
        int j = 1;
        double peso, altura;
        int A = 0, B = 0, C = 0;

        while(j <= trabajadores.length && codigo != 0)
        {
            System.out.println("\nCodigo del Trabajador (ingrese 0 para terminar):");
            codigo = sc.nextInt();

            if(codigo != 0)
            {
                for (int i = 1; i <= trabajadores.length; i++)
                {
                    if (codigo == codigos[i - 1])
                    {
                        if(IMC[i - 1] != 0)
                            System.out.println("Su IMC ya fue calculado previamente");
                        else
                            {
                            System.out.println("\nHola " + trabajadores[i - 1] + ", indicanos tu Peso(Kg): ");
                            peso = sc.nextDouble();
                            System.out.println("Ahora indicanos tu Altura(m): ");
                            altura = sc.nextDouble();

                            // Calculo del valor de IMC
                            IMC[i - 1] = peso / Math.pow(altura, 2);

                            if (IMC[i - 1]<15) {
                                categoria_A[A] = codigos[i - 1];
                                A++;
                            }
                            else if(IMC[i - 1]>=15 && IMC[i - 1]<25) {
                                categoria_B[B] = codigos[i - 1];
                                B++;
                            }
                            else {
                                categoria_C[C] = codigos[i - 1];
                                C++;
                            }
                        }

                        System.out.printf("%s tiene un IMC de: %.2f \n", trabajadores[i - 1], IMC[i - 1]);

                        i = trabajadores.length + 1;
                    }
                }
            }

            j++;
        }

        if(j > 2)
            return 1;
        else
            return 0;
    }
}