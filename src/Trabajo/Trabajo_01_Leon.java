package Trabajo;

import java.util.Scanner;

public class Trabajo_01_Leon
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int Bomba;
        int i;
        int ventas_mayores_a_1000 = 0;
        int ventas_entre_500_y_1000 = 0;
        int ventas_menores_a_500 = 0;
        double CNPB = 0, GMXA = 0, GMXL = 0, Sigma1= 0, Sigma2 = 0;
        double ventatotal = 0;
        double venta;

        System.out.println("Ingresar  #n ventas : ");
        int n = sc.nextInt();

        for (i=1; i<=n; i++)
        {
            System.out.println("Ingresar  valor de venta : ");
            venta = sc.nextDouble();
            System.out.println("Ingrese el tipo de bomba: ");
            Bomba = sc.nextInt();

            //new String[]{"CNPB", "GMXA", "GMXL", "Sigma1", "Sigma2"}, null) + 1;

            if(venta>1000)
                ventas_mayores_a_1000=ventas_mayores_a_1000+1;
            else if(venta>500 && venta<=1000)
                ventas_entre_500_y_1000=ventas_entre_500_y_1000+1;
            else if(venta<=500)
                ventas_menores_a_500=ventas_menores_a_500+1;

            if(Bomba==1)
                CNPB=CNPB+venta;
            else if(Bomba==2)
                GMXA=GMXA+venta;
            else if(Bomba==3)
                GMXL=GMXL+venta;
            else if(Bomba==4)
                Sigma1=Sigma1+venta;
            else if(Bomba==5)
                Sigma2=Sigma2+venta;

            ventatotal=ventatotal+venta;
        }

        System.out.println("Valor de ventas entre 500 y 1000: " + ventas_entre_500_y_1000 + "\n" +
                "Valor de ventas mayores a 1000: " + ventas_mayores_a_1000 + "\n" +
                "Valor de ventas menores a 500: " + ventas_menores_a_500 + "\n" +
                "Valor de monto de CNPB: " + CNPB + "\n" +
                "Valor de monto de GMXA: " + GMXA + "\n" +
                "Valor de monto de GMXL: " + GMXL + "\n" +
                "Valor de monto de SIGMA1: " + Sigma1+ "\n" +
                "Valor de monto de SIGMA2: " + Sigma2 + "\n" +
                "Valor de Venta total: " + ventatotal);
    }
}