package Practicar;
import java.util.Arrays;

public class Ejercicio3 {

    static String [] codigos = {"p054","p067","p026","p039","p012"};
    static String [] nombres = {"Jose","Carlos","Gisella","Sofia","Alberto"};
    static String [] apellidos = {"Garcia","Torres","Jimenez","Gamboa","Segura"};
    static String [] carreras = {"Administracion","Sistemas","Industrial","Negocios","Marketing"};

    public static void main(String[] args)
    {
        //Pruebas unitarias
        int costoCarrera = obtenerCostoCarrera("Administracion");
        System.out.println("El costo de una carrera es: " + costoCarrera);

        double descuento = obtenerDescuentoCarrera("Administracion");
        System.out.println("El descuento por la carrera de " + "Administracion" + "es: " + descuento);

        double cuotaMensual = calcularCuotaMensual("p054");
        System.out.printf("La cuota mensual es: %.2f\n",cuotaMensual);

        String [] alumCuotaAlta = obtenerAlumnoCuotaMasAlta();
        System.out.println("El alumno con la cuota mas alta es: " + Arrays.toString(alumCuotaAlta));
    }

    private static int obtenerCostoCarrera(String carrera) {
        int costo = 0;
        if (carrera.equals("Administracion")){
            costo = 39000;
        }else if(carrera.equals("Sistemas")){
            costo = 45000;
        }else if(carrera.equals("Industrial")){
            costo = 48000;
        }else if(carrera.equals("Negocios")){
            costo = 41000;
        }
        return costo;
    }

    private static double obtenerDescuentoCarrera(String carrera) {
        double desc = 0.0;
        switch (carrera) {
            case "Administracion":
                desc = 0.12;
                break;
            case "Sistemas":
                desc = 0.10;
                break;
            case "Industrial":
                desc = 0.15;
                break;
            case "Negocios":
                desc = 0.08;
                break;
        }
        return desc;
    }

    private static double calcularCuotaMensual(String codAlumno) {
        int costo = 0;
        double dscto = 0.0;

        for(int i=0; i<codigos.length; i++)
        {
            if (codigos[i].equals(codAlumno)){
                costo = obtenerCostoCarrera(carreras[i]);
                dscto = obtenerDescuentoCarrera(carreras[i]);
            }
        }
        double cuota = (costo * (1 - dscto)) / (12 * 4);
        return cuota;
    }

    private static String[] obtenerAlumnoCuotaMasAlta() {
        double mayor = 0.0;
        int posicion = 0;
        double cuota = 0.0;

        for (int i = 0; i < codigos.length; i++) {
            cuota = calcularCuotaMensual(codigos[i]);
            if (cuota > mayor) {
                mayor = cuota;
                posicion = i;
            }
        }

        String [] resultado = new String[2];
        resultado[0] = nombres[posicion] + " " + apellidos[posicion];
        resultado[1] = String.valueOf(mayor);
        return  resultado;
    }
}