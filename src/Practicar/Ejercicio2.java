package Practicar;

import java.util.Arrays;

public class Ejercicio2 {

    static int [] extranjero = {10000, 5000, 200, 800, 60000};
    static int [] nacional = {25000, 18600, 14400, 12000, 30000};

    // Otro set de pruebas
    //static int [] extranjero = {17161, 70998, 804863, 8916, 5};
    //static int [] nacional = {1044511, 501226, 1069270, 23495, 716};

    static String [] platos = {"Lomo Saltado", "Aji de Gallina", "Papa Rellena", "Seco con Frijoles", "Pollo a la Brasa"};

    public static void main(String[] args) {

        int [] totalxPlato = calcularTotalxPlato();
        System.out.println("Los totales de cada plato (Nacional + Extranjero) son: " + Arrays.toString(totalxPlato));

        double [] porcentaje = calcularPorcentajes();
        System.out.println("Los porcentajes de los totales de cada plato son: " + Arrays.toString(porcentaje));

        String plato = platoMasVendido();
        System.out.println("El plato mas vendido es: " + plato);
    }

    private static int[] calcularTotalxPlato() {
        int [] arr = new int[extranjero.length];
        for (int i=0; i<extranjero.length; ++i){
            arr[i] = extranjero[i] + nacional[i];
        }
        return arr;
    }

    private static double[] calcularPorcentajes() {
        int[] totalxPlato = calcularTotalxPlato();
        int suma = 0;

        for (int i=0; i<extranjero.length; i++){
            suma += totalxPlato[i];
        }

        double porcentaje;
        double [] arrPorcentaje = new double[extranjero.length];
        for (int i=0; i<extranjero.length; i++){
            porcentaje = totalxPlato[i] * 100.00 / suma;
            arrPorcentaje[i] =  Math.round(porcentaje*100)/100d;
        }
        return  arrPorcentaje;
    }


    private static String platoMasVendido() {
        int[] totalxPlato = calcularTotalxPlato();
        int cantidadMayor = 0;
        int posicion = 0;

        for (int i = 0; i < extranjero.length; i++) {
            if (totalxPlato[i] > cantidadMayor) {
                cantidadMayor = totalxPlato[i];
                posicion = i;
            }
        }
        return platos[posicion];
    }
}
