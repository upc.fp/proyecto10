package Practicar;

import java.util.Arrays;

public class Ejercicio1a {

    public static void main(String[] args) {

        //int [] ventas = {10,6,14,7,4,5,3};
        //String [] vendedores = {"Hugo","Paco","Luis","Mario","Jorge","David","Manuel"};

        int [] ventas = {15,16,1,0,4,15,13};
        String [] vendedores = {"Roxana","Mariana","Daphne","Jennifer","Lilia","Valeria","Susan"};

        int suma = obtenerTotalVentas(ventas);
        System.out.println("La suma del arreglo ventas es: " + suma);

        double porc = obtenerPorcentajeSuperoCuota(ventas);
        System.out.printf("El porcentaje de vendedores que superaron la cuota es: %.2f\n",porc);

        String [] vendSup = obtenerVendSupCuota(ventas,vendedores);
        //System.out.println("Los agentes que superaron la cuota de ventas son: "+ Arrays.asList(vendSup));
        System.out.println("Los agentes que superaron la cuota de ventas son: "+ Arrays.toString(vendSup));

        int comision = determinarComisionPagar(ventas);
        System.out.println("Pago por comisión: " + comision);
    }

    private static int obtenerTotalVentas(int[] ventas)
    {
        int suma = 0;

        for(int i=0; i<ventas.length; i++)
        {
            suma += ventas[i];
        }

        return suma;
    }

    private static double obtenerPorcentajeSuperoCuota(int[] ventas)
    {
        int cantSup = 0;
        double porc;

        for (int i=0; i<ventas.length; i++)
        {
            if (ventas[i] >= 5)
                cantSup += 1;
        }
        porc = cantSup*100.0/ventas.length;
        return porc;
    }

    private static String[] obtenerVendSupCuota(int[] ventas, String[] vendedores)
    {
        String [] arre_calculo = new String[vendedores.length];

        int contador = 0;

        for (int i=0; i<ventas.length; i++)
        {
            if (ventas[i] >= 5)
            {
                arre_calculo[contador] = vendedores[i];
                contador += 1;
            }
        }

        //Para que solo devuelva un arreglo sin valores nulos
/*        String [] arrF = new String[contador];
        System.arraycopy(arr, 0, arrF, 0, arrF.length);
        return arrF;*/

        return arre_calculo;
    }

    private static int determinarComisionPagar(int[] ventas)
    {
        int totalUnidadesVendidas = obtenerTotalVentas(ventas);
        return totalUnidadesVendidas * 300;
    }
}