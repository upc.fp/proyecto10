package Practicar;

import java.util.Arrays;

public class Ejercicio1b {

    static int [] ventas = {15,16,1,0,4,15,13};
    static String [] vendedores = {"Roxana","Mariana","Daphne","Jennifer","Lilia","Valeria","Susan"};

    //static int [] ventas = {10,6,14,7,4,5,3};
    //static String [] vendedores = {"Hugo","Paco","Luis","Mario","Jorge","David","Manuel"};

    public static void main(String[] args) {

        int suma = obtenerTotalVentas();
        System.out.println("La suma del arreglo ventas es: " + suma);

        double porc = obtenerPorcentajeSuperoCuota();
        System.out.printf("El porcentaje de vendedores que superaron la cuota es: %.2f\n",porc);

        String [] vendSup = obtenerVendSupCuota();
        //System.out.println("Los agentes que superaron la cuota de ventas son: "+ Arrays.asList(vendSup));
        System.out.println("Los agentes que superaron la cuota de ventas son: "+ Arrays.toString(vendSup));

        int comision = determinarComisionPagar();
        System.out.println("Pago por comisión: " + comision);
    }

    private static int obtenerTotalVentas() {
        int suma = 0;
        for(int i=0; i<ventas.length; i++){
            suma += ventas[i];
        }
        return suma;
    }

    private static double obtenerPorcentajeSuperoCuota() {
        int cantSup = 0;
        for (int i=0; i<ventas.length; i++){
            if (ventas[i] >= 5){
                cantSup += 1;
            }
        }
        double porc = cantSup*100.0/ventas.length;
        return porc;
    }

    private static String[] obtenerVendSupCuota() {
        String [] arr = new String[vendedores.length];
        int contador = 0;
        for (int i=0; i<ventas.length; i++){
            if (ventas[i] >= 5){
                arr[contador] = vendedores[i];
                contador += 1;
            }
        }

        //Para que solo devuelva un arreglo sin valores nulos
/*        String [] arrF = new String[contador];
        System.arraycopy(arr, 0, arrF, 0, arrF.length);
        return arrF;*/

        return arr;
    }

    private static int determinarComisionPagar() {
        int totalUnidadesVendidas = obtenerTotalVentas();
        return totalUnidadesVendidas * 300;
    }
}
