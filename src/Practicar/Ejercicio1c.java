package Practicar;

import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio1c {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //int [] ventas = {10,6,14,7,4,5,3};
        //String [] vendedores = {"Hugo","Paco","Luis","Mario","Jorge","David","Manuel"};

        //int [] ventas = {15,16,1,0,4,15,13};
        //String [] vendedores = {"Roxana","Mariana","Daphne","Jennifer","Lilia","Valeria","Susan"};

        System.out.println("Ingresar el tamaño de los arreglos: ");
        int n = sc.nextInt();
        sc.nextLine();

        int [] ventas = new int[n];
        String [] vendedores = new String[n];
        for (int i=0; i<n; i++){
            System.out.println(i+1 + " Ingresar valor: ");
            ventas[i] = sc.nextInt();
            sc.nextLine();
            System.out.println(i+1 + " Ingresar el nombre del vendedor: ");
            vendedores[i] = sc.nextLine();
        }

        int suma = obtenerTotalVentas(ventas);
        System.out.println("La suma del arreglo ventas es: " + suma);

        double porc = obtenerPorcentajeSuperoCuota(ventas);
        System.out.printf("El porcentaje de vendedores que superaron la cuota es: %.2f\n",porc);

        String [] vendSup = obtenerVendSupCuota(ventas,vendedores);
        //System.out.println("Los agentes que superaron la cuota de ventas son: "+ Arrays.asList(vendSup));
        System.out.println("Los agentes que superaron la cuota de ventas son: "+ Arrays.toString(vendSup));

        int comision = determinarComisionPagar(ventas);
        System.out.println("Pago por comisión: " + comision);
    }

    private static int obtenerTotalVentas(int [] ventas) {
        int suma = 0;
        for(int i=0; i<ventas.length; i++){
            suma += ventas[i];
        }
        return suma;
    }

    private static double obtenerPorcentajeSuperoCuota(int[] ventas) {
        int cantSup = 0;
        for (int i=0; i<ventas.length; i++){
            if (ventas[i] >= 5){
                cantSup += 1;
            }
        }
        double porc = cantSup*100.0/ventas.length;
        return porc;
    }

    private static String[] obtenerVendSupCuota(int[] ventas, String[] vendedores) {
        String [] arr = new String[vendedores.length];
        int contador = 0;
        for (int i=0; i<ventas.length; i++){
            if (ventas[i] >= 5){
                arr[contador] = vendedores[i];
                contador += 1;
            }
        }

        //Para que solo devuelva un arreglo sin valores nulos
/*        String [] arrF = new String[contador];
        System.arraycopy(arr, 0, arrF, 0, arrF.length);
        return arrF;*/

        return arr;
    }

    private static int determinarComisionPagar(int[] ventas) {
        int totalUnidadesVendidas = obtenerTotalVentas(ventas);
        return totalUnidadesVendidas * 300;
    }
}
