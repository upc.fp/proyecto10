package Practicar;

public class Ejercicio5 {
    static String [] alumno = {"Denis Pozo","Juan Guerra","Elizabeth Rosas","Roberto Vargas","Mayumi Tineo"};
    static int [] nota = {14,13,12,20,15};
    //static String [] alumno = {"Denis Pozo","Juan Guerra","Elizabeth Rosas","Roberto Vargas","Mayumi Tineo"};
    //static int [] nota = {17,9,12,16,15};
    //static String [] alumno = {"Denis Pozo","Juan Guerra","Elizabeth Rosas","Roberto Vargas","Mayumi Tineo"};
    //static int [] nota = {11,15,20,19,17};

    public static void main(String[] args) {

        double promedio = calcularPromedio();
        System.out.printf("El costo de una carrera es: %.2f\n ",promedio);

        String alumnoMax = alumnoNotaMaxima();
        System.out.println("El alumno con la nota máxima es: " + alumnoMax);

        int supPromedio = superiorPromedio();
        System.out.println("La cantidad de alumnos superior al promedio es: " + supPromedio);
    }

    private static double calcularPromedio() {
        double suma = 0.0;
        for(int i=0; i<nota.length; i++){
            suma += nota[i];
        }
        double resultado = suma/nota.length;
        return resultado;
    }

    private static String alumnoNotaMaxima() {
        double mayor = 0.0;
        int posicion = 0;
        for (int i = 0; i < nota.length; i++) {
            if (nota[i] > mayor) {
                mayor = nota[i];
                posicion = i;
            }
        }
        return alumno[posicion];
    }

    private static int superiorPromedio() {
        double promedio = calcularPromedio();
        int contador = 0;
        for (int i = 0; i < nota.length; i++) {
            if (nota[i] > promedio) {
                contador += 1;
            }
        }
        return contador;
    }
}
