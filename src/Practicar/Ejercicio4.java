package Practicar;

import java.util.Arrays;

public class Ejercicio4
{
    public static void main(String[] args)
    {
        double [] puntaje_escondite = {3,1,5,2,5};
        double [] promedio = {3.6,1.4,4.5,2.2,5};
        String [] participante = {"El escondite","Pintamar","Fuerza Mancora","Jorge 2","Embarcadero"};
        //double [] puntaje = {2.3,1.2,4.3,2.5,4.1};
        //String [] participante = {"El escondite","Pintamar","Fuerza Mancora","Jorge 2","Embarcadero"};
        //double [] puntaje = {1.1,1.2,3.2,2.7,3.5};
        //String [] participante = {"El escondite","Pintamar","Fuerza Mancora","Jorge 2","Embarcadero"};

        double puntajePromedio = obtenerPuntajePromedio(puntaje_escondite);
        System.out.println("El puntaje promedio es: " + puntajePromedio);

        double porcentajeExc = calcularPorcentajeExcelente(promedio);
        System.out.printf("El porcentaje de excelente es: %.2f\n",porcentajeExc);

        String [] peores = buscarMalosPesimos(promedio,participante);
        System.out.println("Los peores participantes son: " + Arrays.toString(peores));
    }

    private static double obtenerPuntajePromedio(double [] puntaje)
    {
        double suma = 0.0;

        for(int i=0; i<puntaje.length; i++)
        {
            suma += puntaje[i]; // suma = suma + puntaje[i]
        }

        double resultado = suma/puntaje.length;

        return resultado;
    }

    private static double calcularPorcentajeExcelente(double[] puntaje)
    {
        int contador = 0;

        for(int i=0; i<puntaje.length; i++)
        {
            if (puntaje[i] == 5)
            {
                contador += 1; // contador = contador + 1   <=>  contador++;
            }
        }

        return contador*100.00/puntaje.length;
    }

    private static String[] buscarMalosPesimos(double[] puntaje, String[] participante)
    {
        String [] arr = new String[participante.length];
        int pos = 0;

        for(int i=0; i<puntaje.length; i++)
        {
            if (puntaje[i] <= 2)
            {
                arr[pos] = participante[i];
                pos += 1;
            }
        }
        //Para que solo devuelva un arreglo sin valores nulos
/*        String [] arrF = new String[pos];
        System.arraycopy(arr, 0, arrF, 0, arrF.length);
        return arrF;*/

        return arr;
    }
}