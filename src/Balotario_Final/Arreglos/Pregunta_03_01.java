package Balotario_Final.Arreglos;

import java.util.Arrays;

public class Pregunta_03_01
{
    static int [] ventanilla_1 = {0, 1, 4, 5};
    static int [] ventanilla_2 = {3, 2};
    static int [] ventanilla_3 = {0, 3, 0, 1, 0, 3};
    static int [] ventanilla_4 = {0, 1, 2};
    static int [] ventanilla_5 = {4, 4, 3, 4, 0};

    static String [] asistentes = {"Luis", "Felipe", "Esteban", "Maria", "Carmen"};

    public static void main(String[] args)
    {
        int cantidad, total;
        String [] asistente_top;

        cantidad = cantidad_encuesta();
        System.out.println("1. Hay " + cantidad + " Pacientes que contestaon la encuesta");

        total = promedio_satisfaccion();
        System.out.println("2. El nivel de satisacción de Recepción es " + total);

        asistente_top = identificar_top();
        System.out.println("3. El asistente con mayor satisfaccion es: "+ Arrays.toString(asistente_top));
    }

    public static int cantidad_encuesta()
    {
        int total;

        total = ventanilla_1.length + ventanilla_2.length + ventanilla_3.length + ventanilla_4.length + ventanilla_5.length;

        return total;
    }

    public static int promedio_satisfaccion()
    {
        int suma_total;
        suma_total = suma_arreglo(ventanilla_1) +
                     suma_arreglo(ventanilla_2) +
                     suma_arreglo(ventanilla_3) +
                     suma_arreglo(ventanilla_4) +
                     suma_arreglo(ventanilla_5);

        return suma_total;
    }

    public static int suma_arreglo(int [] ventanilla)
    {
        int total = 0;

        for(int i = 0; i < ventanilla.length; i++)
        {
            total += ventanilla[i];
        }

        return total;
    }

    public static String [] identificar_top()
    {
        String [] top = new String[2];
        int maximo = 0, i_max = 0, i = 1;

        while(i<=5)
        {
            switch(i){
                case 1: if(maximo < suma_arreglo(ventanilla_1)) {
                            maximo = suma_arreglo(ventanilla_1);
                            i_max = i-1;
                        }
                        break;

                case 2: if(maximo < suma_arreglo(ventanilla_2)){
                            maximo = suma_arreglo(ventanilla_2);
                            i_max = i-1;
                        }
                        break;

                case 3: if(maximo < suma_arreglo(ventanilla_3)){
                            maximo = suma_arreglo(ventanilla_3);
                            i_max = i-1;
                        }
                        break;

                case 4: if(maximo < suma_arreglo(ventanilla_4)){
                            maximo = suma_arreglo(ventanilla_4);
                            i_max = i-1;
                        }
                        break;

                case 5: if(maximo < suma_arreglo(ventanilla_5)){
                            maximo = suma_arreglo(ventanilla_5);
                            i_max = i-1;
                        }
                        break;
            }
            i++;
        }

        top[0] = asistentes[i_max];
        top[1] = String.valueOf(maximo);

        return top;
    }
}