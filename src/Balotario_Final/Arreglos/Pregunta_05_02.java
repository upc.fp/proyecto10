package Balotario_Final.Arreglos;

import java.util.Arrays;

public class Pregunta_05_02 {

    static String[] categorias = {"1", "2", "3", "B", "N"};
    static int[] votos = {380, 350, 270, 200, 100};

    public static void main(String[] args)
    {
        //Pregunta 01
        int cant_personas;
        cant_personas = hallar_votantes();
        System.out.println("\nLa cantidad de votantes es: "+ cant_personas);

        //Pregunta 02
        String[] voto_BN;
        voto_BN = cantidad_votos_BN();
        System.out.println("La cantidad y porcentaje de votos Blancos y Nulos es: " + Arrays.toString(voto_BN));

        //Pregunta 03
        String[] mas_votos;
        mas_votos = hallar_mas_votado();
        System.out.println("La lista con mas votación es: " + Arrays.toString(mas_votos));

        //Pregunta 04
        String[] votos_top1_top2;
        votos_top1_top2 = hallar_top1_top2(mas_votos);
        System.out.println("Las dos listas mas votadas y sus porcentajes son: " + Arrays.toString(votos_top1_top2));
    }

    static int hallar_votantes()
    {
        int total = 0;

        for(int i=0; i<votos.length; i++)
        {
            total += votos[i];
        }

        return total;
    }


    static String[] cantidad_votos_BN()
    {
        String[] cantidad_BN = new String[2];
        double porcentaje;
        int total_votantes;

        cantidad_BN[0] = String.valueOf(votos[3] + votos[4]);
        total_votantes = hallar_votantes();

        porcentaje = (Double.parseDouble(cantidad_BN[0]) * 100) / total_votantes;
        porcentaje = Math.round(porcentaje*100.0)/100.0;

        cantidad_BN[1] = String.valueOf(porcentaje);

        return cantidad_BN;
    }

    static String[] hallar_mas_votado()
    {
        String[] mas_votado = new String[2];
        int mayor = 0, i_max = 0, total_votantes;
        double porcentaje;

        for(int i=0; i<3; i++)
        {
            if(mayor < votos[i])
            {
                mayor = votos[i];
                i_max = i;
            }
        }

        total_votantes = hallar_votantes();
        porcentaje = (double) (mayor * 100) / total_votantes;
        porcentaje = Math.round(porcentaje*100.0)/100.0;

        mas_votado[0] = categorias[i_max];
        mas_votado[1] = String.valueOf(porcentaje);

        return mas_votado;
    }

    static String[] hallar_top1_top2(String[] top_1)
    {
        String[] top1_top2 = new String[4];
        int mayor_1 = 0, mayor_2 = 0, i_max_2 = 0, total_votantes_sin_BN;
        double porcentaje_1, porcentaje_2;

        mayor_1 = votos[Integer.parseInt(top_1[0])-1];

        for(int i=0; i<3; i++)
        {
            if(mayor_2 < votos[i] && votos[i] != mayor_1)
            {
                mayor_2 = votos[i];
                i_max_2 = i;
            }
        }

        total_votantes_sin_BN = hallar_votantes() - votos[3] - votos[4];

        porcentaje_1 = (double) (mayor_1 * 100) / total_votantes_sin_BN;
        porcentaje_1 = Math.round(porcentaje_1*100.0)/100.0;

        porcentaje_2 = (double) (mayor_2 * 100) / total_votantes_sin_BN;
        porcentaje_2 = Math.round(porcentaje_2*100.0)/100.0;

        top1_top2[0] = top_1[0];
        top1_top2[1] = String.valueOf(porcentaje_1);

        top1_top2[2] = categorias[i_max_2];
        top1_top2[3] = String.valueOf(porcentaje_2);

        return top1_top2;
    }
}