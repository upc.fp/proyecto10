package Balotario_Final.Arreglos;

import java.util.Arrays;

public class Pregunta_03_02 {

    static int [] ventanilla_1 = {0, 2, 1, 2, 2, 2, 1};
    static int [] ventanilla_2 = {1, 3, 1, 2, 3};
    static int [] ventanilla_3 = {2, 2, 2, 3};
    static int [] ventanilla_4 = {4, 2, 2};
    static int [] ventanilla_5 = {3, 4, 3, 4, 2};
    static String [] asistentes = {"Raul Osorio", "Diana Padilla", "Ana Galvez", "Elias Flores", "Ebert Diaz"};

    public static void main(String[] args)
    {
        int cant_pacientes;
        cant_pacientes = contar_pacientes();
        System.out.println("Cantidad de Pacientes: "+cant_pacientes);

        double porcen_satisfaccion;
        porcen_satisfaccion = porcentaje_satisfacccion(cant_pacientes);
        System.out.println("Porcentaje de Satisfacción: " + porcen_satisfaccion);

        String [] asistente_top;
        asistente_top = calcular_top();
        System.out.println("Asistente TOP 1: "+ Arrays.toString(asistente_top));
    }

    static int contar_pacientes()
    {
        int cantidad;
        cantidad = ventanilla_1.length +
                   ventanilla_2.length +
                   ventanilla_3.length +
                   ventanilla_4.length +
                   ventanilla_5.length;
        return cantidad;
    }

    static double porcentaje_satisfacccion(int total_pacientes)
    {
        int total_satisfaccion;
        double porcentaje;
        total_satisfaccion = cant_satisfecho(ventanilla_1) +
                             cant_satisfecho(ventanilla_2) +
                             cant_satisfecho(ventanilla_3) +
                             cant_satisfecho(ventanilla_4) +
                             cant_satisfecho(ventanilla_5);

        porcentaje = (double) total_satisfaccion * 100 / total_pacientes;
        porcentaje = Math.round(porcentaje*100.0)/100.0;

        return porcentaje;
    }

    static int cant_satisfecho(int [] ventanilla)
    {
        int satisfecho = 0;
        for(int i = 0; i < ventanilla.length; i++)
        {
            if(ventanilla[i] == 3 || ventanilla[i] == 4)
                satisfecho++;
        }
        return satisfecho;
    }

    static String[] calcular_top()
    {
        double mayor = 0;
        double satisfecho;
        int indice = -1;
        String [] top_1 = new String[2];

        satisfecho = nivel_satisfaccion(ventanilla_1);
        if(mayor < satisfecho)
        {
            mayor = satisfecho;
            indice = 0;
        }

        satisfecho = nivel_satisfaccion(ventanilla_2);
        if(mayor < satisfecho)
        {
            mayor = satisfecho;
            indice = 1;
        }

        satisfecho = nivel_satisfaccion(ventanilla_3);
        if(mayor < satisfecho)
        {
            mayor = satisfecho;
            indice = 2;
        }

        satisfecho = nivel_satisfaccion(ventanilla_4);
        if(mayor < satisfecho)
        {
            mayor = satisfecho;
            indice = 3;
        }

        satisfecho = nivel_satisfaccion(ventanilla_5);
        if(mayor < satisfecho)
        {
            mayor = satisfecho;
            indice = 4;
        }

        top_1[0] = asistentes[indice];
        top_1[1] = String.valueOf(mayor); //convertir Duoble en String

        return top_1;
    }

    static double nivel_satisfaccion(int[] ventanilla)
    {
        int cant_satisfaccion = cant_satisfecho(ventanilla);
        double porcentaje;

        porcentaje = (double) cant_satisfaccion * 100 / ventanilla.length;
        porcentaje = Math.round(porcentaje*100.0)/100.0;

        return porcentaje;
    }
}