package Balotario_Final.Arreglos;

import java.util.Arrays;

public class Pregunta_05_01 {

    static String [] vector_1 = {"1", "2", "3", "B", "N"};
    static int    [] vector_2 = { 50,  770,  180,  300,  200};

    public static void main(String[] args)
    {
        int cantidad_personas;
        cantidad_personas = contar_total_personas();
        System.out.println("Votantes: " + cantidad_personas);

        double [] blanco_nulo;
        blanco_nulo = porcentaje_blanco_nulo(cantidad_personas);
        System.out.println("Cantidad y Porcentaje de Blancos y Nulos: "+ Arrays.toString(blanco_nulo));

        String [] top_1;
        top_1 = calcular_ganador(cantidad_personas);
        System.out.println("LISTA GANADORA: "+ Arrays.toString(top_1));

        String [] top_1_2;
        top_1_2 = calcular_top_1_2(top_1);
        System.out.println("LISTA TOP 1 y TOP 2: "+ Arrays.toString(top_1_2));
    }

    static int contar_total_personas()
    {
        int total = 0;

        for(int i=0; i<vector_2.length; i++)
        {
            total += vector_2[i]; // total = total + vector_2[i]
        }

        return total;
    }

    static double[] porcentaje_blanco_nulo(int total_votos)
    {
        double [] BN = new double[2];

        BN[0] = vector_2[3] + vector_2[4]; // cantidad
        BN[1] = (BN[0]*100)/total_votos;   // porcentaje
        BN[1] = Math.round(BN[1]*100.0)/100.0;

        return BN;
    }

    static String[] calcular_ganador(int total_votos)
    {
        String [] ganador = new String[2];
        int mayor = 0, i_max = 0;
        double porcentaje;

        for(int i=0; i<3; i++)
        {
            if(mayor < vector_2[i])
            {
                mayor = vector_2[i];
                i_max = i;
            }
        }

        porcentaje = ((double)mayor*100)/(double)total_votos;
        porcentaje = Math.round(porcentaje*100.0)/100.0;

        ganador[0] = vector_1[i_max];
        ganador[1] = String.valueOf(porcentaje);

        return ganador;
    }

    static String[] calcular_top_1_2(String [] top_1)
    {
        String [] top_1_2 = new String[4];
        int mayor_1 = 0, mayor_2 = 0, i_max_2 = 0, total_votos_sin_BN = 0;
        double porcentaje_1, porcentaje_2;

        mayor_1 = vector_2[Integer.parseInt(top_1[0])-1];

        for(int i=0; i<3; i++)
        {
            if(mayor_2 < vector_2[i] && vector_2[i] < mayor_1)
            {
                mayor_2 = vector_2[i];
                i_max_2 = i;
            }
            total_votos_sin_BN += vector_2[i];
        }

        porcentaje_1 = ((double)mayor_1*100)/(double)total_votos_sin_BN;
        porcentaje_1 = Math.round(porcentaje_1*100.0)/100.0;

        porcentaje_2 = ((double)mayor_2*100)/(double)total_votos_sin_BN;
        porcentaje_2 = Math.round(porcentaje_2*100.0)/100.0;

        top_1_2[0] = top_1[0];
        top_1_2[1] = String.valueOf(porcentaje_1);
        top_1_2[2] = vector_1[i_max_2];
        top_1_2[3] = String.valueOf(porcentaje_2);

        return top_1_2;
    }
}