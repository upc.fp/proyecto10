package Balotario_Final.Arreglos;

import java.util.Arrays;

public class Prueba_01 {

    static String [] textos_repetidos = {"casa","casa","mesa","puerta","mesa","casa","mesa","puerta","mesa","mesa"};

    public static void main(String[] args) {
        String [] textos_unicos;
        textos_unicos = filtrar_unicos();
        System.out.println("\nArreglo con Textos Unicos: "+ Arrays.toString(textos_unicos));
    }

    static String [] filtrar_unicos()
    {
        String [] vector_unico = new String[textos_repetidos.length];
        int encontro_igual = 0;
        int i, j, k = 0;

        for(i=0; i < textos_repetidos.length; i++)
        {
            for(j=0; j < vector_unico.length; j++)
            {
                if(textos_repetidos[i] == vector_unico[j])
                    encontro_igual = 1;
            }

            if(encontro_igual != 1)
            {
                vector_unico[k] = textos_repetidos[i];
                k++;
            }

            encontro_igual = 0;
        }

        String [] vector_retorno = new String[k];
        for(i=0; i<k; i++)
            vector_retorno[i] = vector_unico[i];

        return vector_retorno;
    }
}
