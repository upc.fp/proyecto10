package Balotario_Final.Arreglos;

import java.util.Scanner;

public class Pregunta_01
{
    //static String[] candidatos = {"KF", "PK"};
    //static String[] hoja_vida = {"EL", "FA", "CP", "IR", "RS","PG"};

    static String[] columna_1 = {"KF", "KF", "KF","KF", "KF", "KF", "PK", "PK", "PK", "PK", "PK", "PK"};
    static String[] columna_2 = {"EL", "FA", "CP", "IR", "RS", "PG", "EL", "FA", "CP", "IR", "RS", "PG"};
    static int[]    columna_3 = {350 ,122 ,431 ,101 ,341 ,78 ,450 ,121 ,452 ,123 ,111 ,345};

    public static void main(String[] args) {
        int puntaje;
        String iniciales_candidato;
        String[] candidato_ganador;
        Scanner sc = new Scanner(System.in);

        System.out.println("Ingrese las iniciales del candidato");
        iniciales_candidato = sc.nextLine();

        puntaje = puntaje_obtenido(iniciales_candidato);
        System.out.println("\nEl puntaje obtenido por el candidato '"+ iniciales_candidato +"' es: "+ puntaje);

        candidato_ganador = hallar_mayor_puntaje();
        System.out.println("\nEl candidato ganador es '"+ candidato_ganador[0] + "' con un puntaje de "+ candidato_ganador[1]);

        String codigo_HV_mayor;
        codigo_HV_mayor = codigo_mayor();
        System.out.println("\nEl codigo que obtuvo el mayor puntaje es '"+ codigo_HV_mayor + "'");
    }

    static int puntaje_obtenido(String candidato)
    {
        int puntaje_candidato = 0;

        for(int i = 0; i < columna_1.length; i++)
        {
            if(candidato.equals(columna_1[i]))
                puntaje_candidato += columna_3[i];
        }

        return puntaje_candidato;
    }

    static String[] hallar_mayor_puntaje()
    {
        String[] ganador = new String[2];
        String[] candidatos_unicos;

        int mayor = 0, puntaje = 0;
        candidatos_unicos = filtrar_unicos(columna_1);

        for(int i=0; i < candidatos_unicos.length; i++)
        {
            puntaje = puntaje_obtenido(candidatos_unicos[i]);
            if(mayor < puntaje)
            {
                mayor = puntaje;
                ganador[0] = candidatos_unicos[i];
            }
        }

        ganador[1] = String.valueOf(mayor);

        return ganador;
    }

    static String codigo_mayor()
    {
        int mayor = 0;
        String codigo = "";

        for(int i=0; i < columna_3.length; i++)
        {
            if(mayor < columna_3[i]) {
                mayor = columna_3[i];
                codigo = columna_2[i];
            }
        }

        return codigo;
    }

    static String [] filtrar_unicos(String [] textos_repetidos)
    {
        String [] vector_unico = new String[textos_repetidos.length];
        int encontro_igual = 0;
        int i, j, k = 0;

        for(i=0; i < textos_repetidos.length; i++)
        {
            for(j=0; j < vector_unico.length; j++)
            {
                if(textos_repetidos[i] == vector_unico[j])
                    encontro_igual = 1;
            }

            if(encontro_igual != 1)
            {
                vector_unico[k] = textos_repetidos[i];
                k++;
            }

            encontro_igual = 0;
        }

        String [] vector_retorno = new String[k];
        for(i=0; i<k; i++)
            vector_retorno[i] = vector_unico[i];

        return vector_retorno;
    }
}